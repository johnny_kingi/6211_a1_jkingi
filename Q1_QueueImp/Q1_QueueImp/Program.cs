﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Q1_QueueImp
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue q = new Queue();

            int i = 1;

            while (i == 1)
            {
                try
                {
                    Console.Clear();
                    Console.WriteLine("Please enter menu choice:");
                    Console.WriteLine();
                    Console.WriteLine("Choice 1: Show items in your Queue");
                    Console.WriteLine("Choice 2: Add Items to Queue");
                    Console.WriteLine("Choice 3: Remove object from beginning of Queue");
                    Console.WriteLine("Choice 4: Check if item is in the Queue");
                    Console.WriteLine("Choice 5: Make a copy of current Array");
                    Console.WriteLine("Choice 6: Exit\n\n");
                    int menuchoice = int.Parse(Console.ReadLine());

                    switch (menuchoice)
                    {
                        case 1:
                            MyQueue.printQueue(q);
                            break;
                        case 2:
                            MyQueue.Enqueue(q);
                            break;
                        case 3:
                            MyQueue.Dequeue(q);
                            break;
                        case 4:
                            MyQueue.Contains(q);
                            break;
                        case 5:
                            MyQueue.ToArray(q);
                            break;
                        case 6:
                            Console.WriteLine("\nPress any key to exit... Good Bye - Have a nice day :)");
                            i = 2;
                            break;
                    }
                    Console.ReadKey();
                }
                catch (Exception Ex)
                {
                    Console.WriteLine("\n\n*****  Error  *****\n\n");
                    Console.WriteLine(Ex.ToString());
                    Console.WriteLine("\n\nOops something went wrong invalid entry try again, press any key to continue...");
                    Console.ReadKey();
                }
            }

        }            
    }
    class MyQueue //MyQueue Class containing methods
    {
        public static void printQueue(Queue q) // Print Queue to display all items in Queue
        {
            if (q.Count == 0) // if Queue is empty, print to advise
            {
                Console.WriteLine("\t\nQueue empty, please add something to the Queue first...\n");
            }
            else
            {
                Console.WriteLine("\nCurrent queue: ");
                foreach (string c in q) Console.WriteLine(c + " ");
            }
            Console.WriteLine("\nPress any key to continue...");
        }
        public static void Enqueue(Queue q)//Add objects to end of queue
        {
            Console.WriteLine("\nPlease enter a value to be added to Queue: ");
            string input = Console.ReadLine();
            Console.WriteLine("You have added {0} to the queue", input);
            q.Enqueue(input);
            printQueue(q);
        } 
        public static void  Dequeue(Queue q)//Removes and returns the object to beginning of Queue
        {          
            string value = (string)q.Dequeue();
            Console.WriteLine("\n{0} has been removed from front of Queue", value);
            printQueue(q);
        }
        public static void Contains(Queue q)//Determines whether an element is in the Queue
        {
            Console.WriteLine("\nEnter value to be checked if it exists in array: ");
            string item = Console.ReadLine();
            if (q.Contains(item))
            {
                Console.WriteLine("Yes, {0} is in the queue", item);
            }
            else
            {
                Console.WriteLine("no, {0} is not in the queue", item);
            }
            Console.WriteLine("\n\nPress any key to return to menu...");
        }
        public static void ToArray(Queue q) // copy of "q" array to copyArray
        {
            Queue copyQueue = new Queue(q.ToArray());  //copy original Queue (q) to new Queue (copyQueue)

            Console.WriteLine("\nCopy of array complete, Items in copied array: ");
            foreach (string c in copyQueue) Console.WriteLine(c + " ");
            Console.WriteLine("\n\nPress any key to return to menu...");
        }
    }
}