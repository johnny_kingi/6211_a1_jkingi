﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Q4_Evaluate_Postfix
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\n\tPlease insert valid postfix equation:\n");
            string input = Console.ReadLine();  // take in valid postfix expression

            Console.WriteLine("\n\tYou entered\t:  " + input);  //equation before calculation
            input += ")";  //apend to end of input string        

            PostfixEvaluator.EvaluatePostfixExpression(input);  //invoke method to calculate equation
        
            Console.WriteLine("\n\tThe result of this postfix equation is:\t" +PostfixEvaluator.result);  //calculated postfix expression
            Console.ReadLine();
        }

        
        class PostfixEvaluator // class with methods to calculate postfix equation
        {
            public static Stack s = new Stack(); //Stack to push numbers
            public static double result;  //string to contain result

            public static void EvaluatePostfixExpression(string input)  //method to take in input and make calculation
            {
                for (int i = 0; i < input.Length; i++) //iterate over each char of input
                {
                    char c = input[i];  //convert each letter of input string to char
                    
                    if (c ==')') // end when ) is found in string
                    {
                        break;
                    }

                    else if (char.IsNumber(c) == true)    //if char == number, append char to postfix
                    {
                        s.Push(c);
                    }
                    else if (IsOperator(c) == true)  //else if c is opeartor
                    {
                        char op = c;
                        int x = int.Parse(s.Pop().ToString());  //make x top item of stack and convert to int
                        int y = int.Parse(s.Pop().ToString());  //make y top item of stack and convert to int
                        //Console.WriteLine("var x:  {0}\tvar y:  {1}", x,y);

                        Calculate(x, y,c);  //invoke calculation method
                    }
                }
            }

            public static double Calculate(int x, int y,char op)
            {
                switch (op)
                {
                    case '+':
                        result = y + x;
                        break;
                    case '-':
                        result = y - x;
                        break;
                    case '*':
                        result = y * x;
                        break;
                    case '/':
                        result = y / x;
                        break;
                    case '^':
                        result = Math.Pow(y, x);
                        break;
                    case '%':
                        result = y % x;
                        break;
                }

                //Console.WriteLine("Result of calculation =\t" +result);
                s.Push(result);
                return result;
            }
            public static bool IsOperator(char c)  //IsOperator method to check if value is an operator, return bool
            {
                if (c == '+' || c == '-' || c == '*' || c == '/' || c == '^' || c == '%' || c == '^')
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
