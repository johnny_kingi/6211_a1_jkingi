﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrecV2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\n\n\tPlease enter a valid single digit infix expression to be converted to postfix:\n");
            string input = Console.ReadLine();  //taking in user input equation (assuming valid entry)

            infixToPostfix.s.Push('('); // push ( to stack
            input += ")";   //append ) to infix

            infixToPostfix.infixPostfix(input);  //invoke infixToPostfix method take in input

            Console.WriteLine("\n\tYour expression converted to postfix is:\t" +infixToPostfix.postfix); //print converted input
            Console.WriteLine("\n\tPress any key to exit...\n");
            Console.ReadKey(); //end program
        }

        class infixToPostfix //class containing methods
        {
            public static Stack s = new Stack(); //declare public variables to be used
            public static string postfix = " ";
            public static int Prec;

            public static string infixPostfix(string input) //infixPostfix method to convert input
            {
                for (int i = 0; i < input.Length; i++) //iterate over each char of input
                {
                    char c = input[i];  //convert each letter of input string to char
                    if (char.IsNumber(c) == true)   //if char == number, append char to postfix
                    {
                        postfix += c;
                    }

                    else if (c == '(') // else if c == ( push to stack
                    {
                        s.Push(c);
                    }

                    else if (c == ')') // else if c == ) append s.pop to postfix until ( is at top of stack
                    {                        
                            if (s.Peek().ToString()[0] == '(')
                            {
                                s.Pop();
                            }
                            else
                            {
                                postfix += s.Pop();
                            }                        
                    }

                    else if (IsOperator(c) == true) //if c from input string is operator == true
                    {
                        if (s.Count <= 0) //if stack empty, push operator to stack
                        {
                            s.Push(c);
                        }
                        else //else if operator in stack, get Precedence
                        {
                            Precedence(c, i);  //invoke Precedence method to give operator at top of stack precedence

                            if (Prec == 2) //operator at top of stack has highest precedence so pop and append to postfix
                            {
                                postfix += s.Pop();
                                i--;
                            }
                            else if (Prec == 1) //Operator with precedence 1 at top of stack, Compare and either push or pop to postfix
                            {
                                if (c == '^')
                                {
                                    s.Push(c);
                                }
                                else if (c == '+' || c == '-')
                                {
                                    postfix += s.Pop();
                                    i--;
                                }
                                else
                                {
                                    postfix += s.Pop();
                                    i--;
                                }
                            }
                            else
                            {
                                if (c == '+' || c == '-') // Lowest Prec operator at top of stack
                                {
                                    if (s.Peek().ToString() == "(") //if ( in top of stack Pop and discard
                                    {
                                        s.Pop();
                                    }
                                    else 
                                        postfix += s.Pop();
                                        s.Push(c);
                                }
                                else  //push higher precedence item of input to stack
                                {
                                    s.Push(c);
                                }
                            }
                        }
                    }
                }
                
                while (s.Count != 0) // append last item of stack to postfix
                {
                    postfix += s.Pop();
                }

                return postfix;  //return converted postfix result to main method
            }

            public static int Precedence(char c, int i) //Preccedence method to give precedence to higher priority operator
            {
                var op2 = s.Peek().ToString()[0]; //convert item at top of stack to char

                if (op2 == '^') //Set operator in stack with highest Precedence
                {
                    Prec = 2;
                }
                else if (op2 == '*' || op2 == '/' || op2 == '%')
                {
                    Prec = 1;
                }
                else //Set opoerator in stack with lowest precedence
                {
                    Prec = 0;
                }

                return Prec;
            }
            public static bool IsOperator(char c)  //IsOperator method to check if value is an operator, return bool
            {
                if (c == '+' || c == '-' || c == '*' || c == '/' || c == '^' || c == '%' || c == '^')
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }

}