﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Q2_Palindromes
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack _stack = new Stack();
            Stack _stack2 = new Stack();
            Stack _stack3 = new Stack();
            int i = 1;

            do
            {
                try
                {
                    Console.Clear();
                    palindromeCheck(_stack, _stack2, _stack3); //invoke method

                    Console.Clear();
                    Console.WriteLine("\n\t1 to Enter another word\n");
                    Console.WriteLine("\n\t2 to exit\n");
                    i = int.Parse(Console.ReadLine());
                }

                catch (Exception Ex) //return error for incorrect input and loop back to start
                {
                    i = 1;
                    Console.WriteLine("\n\n*****  Error  *****\n\n");
                    Console.WriteLine(Ex.ToString());
                    Console.WriteLine("\n\nOops something went wrong invalid entry try again, press any key to continue...");
                    Console.ReadKey();
                }
            } while (i == 1);
        }

        static void palindromeCheck(Stack _stack, Stack _stack2, Stack _stack3)
        {
            Console.WriteLine("\n\tPlease enter a word to check if it is a palindrome");
            var input = Console.ReadLine();

            input = input.ToLower(); //ignore capitlization
            input = Regex.Replace(input, @"[^\w]", string.Empty); //remove all Punctuation

            foreach (char i in input)//adding chars of input individually into stack array 
            {
                _stack.Push(i);
            }            

            _stack2 = new Stack(_stack.ToArray());//Copying Stack to _stack2
            Console.WriteLine();

            foreach (var item in _stack2)//using _stack3 to take items back into stack in original order
            {
                _stack3.Push(item);
            }
;
            while (_stack2.Count > 0)
            {
                var peek1 = _stack2.Peek();
                var peek2 = _stack3.Peek();
                Console.WriteLine("{0}  {1}",peek1,peek2);
                
                if (peek1.ToString() == peek2.ToString()) // compare char from each Queue and pop if match 
                {
                    _stack2.Pop();
                    _stack3.Pop();
                }
                else // else Clear _stack2 to exit loop
                {
                    _stack2.Clear();
                }
            }

            if (_stack3.Count == 0) // if all items popped to 0 then == Palindrome
            {
                Console.WriteLine("\n\nYes the word you entered is a Palindrome");

            }
            else // else not a Palindrome
            {
                Console.WriteLine("\n\nNope not a Palindrome");
            }

            _stack.Clear(); // clear all stacks for next entry
            _stack2.Clear();
            _stack3.Clear();

            Console.ReadKey();
        }
    }
}
